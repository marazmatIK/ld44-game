-- Include Simple Tiled Implementation into project
Object = require 'lib/classic/classic'
Timer = require 'lib/hump/timer'

sti = require "lib/sti"
bump = require "lib/bump"
Event = require "lib/event"
deep = require "lib/deep/deep"
gamera = require "lib/gamera/gamera"
Camera = require "lib/hump/camera"

HC = require 'lib/hardoncollider'

log = require "lib/log/log"
