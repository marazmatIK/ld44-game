PROJECTILE_VELOCITY = 320
COLLECTABLE_VELOCITY = 120
PLAYER_MAX_VELOCITY = 400
GRAVITY = 300
ANGLE_VELOCITY = math.pi

SCORE_INCREMENT = 10

MAX_SOULS_COUNT = 8

CATS_SPAWN_CHANCE = 0.25

POINTS_BOOST_CHANCE = 0.2

TILE_SIZE = 64

SCREEN_WIDTH = love.graphics.getWidth( )
SCREEN_HEIGHT = love.graphics.getHeight( )
JUMP_HEIGHT = 300

GAME_DURATION = 3

function projectileShape (radius)
    return {a = {x = 0 - radius, y = 0 + radius / 2},
        b = {x = 0 - radius, y = 0 - radius / 2},
        c = {x = 0 + radius * 0.55, y = 0 - radius / 2},
        d = {x = 0 + radius, y = 0},
        e = {x = 0 + radius * 0.55, y = 0 + radius / 2}}
end