require "objects/Game"
require "objects/Introducer"
require 'objects/SoundRegistry'
require 'objects/LevelManager'
require 'Utils'

function love.load()
    LEVEL_MANAGER = LevelManager()
    restartGameObject(false)
    game.active = false
    intro = Introducer({x = SCREEN_WIDTH / 4, y = SCREEN_HEIGHT / 3, onIntroEnd = function()
        game.active = true
        intro:stopMusic()
        game:startGame()
        intro.active = false
    end})
    intro:startMusic()
end

function love.keypressed( key, isrepeat )
    if key == 'f' and game.active == true then
        restartGameObject(true)
    end

    if key == 'return' and game.active == true then

        LEVEL_MANAGER:nextLevel()
        restartGameObject(true)
    end

    game:keypressed(key, isrepeat)
    intro:keypressed(key, isrepeat)
end

function restartGameObject(start)
    if game then
        game:clearResources()
    end
    -- TODO: Refactor that to reusable single method and use in 3 initializator places
    game = Game({scoresToWin = LEVEL_MANAGER:currentLevel().scoresToWin,
        catsSpawnChance = LEVEL_MANAGER:currentLevel().catsSpawnChance,
        timer = LEVEL_MANAGER:currentLevel().timer,
        hud = HudIndicator({levelManager = LEVEL_MANAGER})}
    )
    if start == true then
        game:startGame()
    end

    return game
end

function love.keyreleased( key )
    game:keyreleased(key)
end

function love.update(dt)

    game:update(dt)
    intro:update(dt)

    if love.keyboard.isDown('escape') then
        love.event.quit()
    end
end

function love.draw()
    intro:draw()

    red = 33/255
    green = 4/255
    blue = 44/255
    alpha = 100/100
    love.graphics.setBackgroundColor( red, green, blue)

    game:draw()
end
