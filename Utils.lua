function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function drawShape(x, y, angle, shape)

    love.graphics.setColor(1, 1, 1)

    local cos = math.cos(angle)
    local sin = math.sin(angle)


    love.graphics.line( (x + rotateX(shape.a, cos, sin)), 1 * (y + rotateY(shape.a, cos, sin)),

        1 * (x + rotateX(shape.b, cos, sin)), 1 * (y + rotateY(shape.b, cos, sin)),

        1 * (x + rotateX(shape.c, cos, sin)), 1 * (y + rotateY(shape.c, cos, sin)),

        1 * (x + rotateX(shape.d, cos, sin)), 1 * (y + rotateY(shape.d, cos, sin)),

        1 * (x + rotateX(shape.e, cos, sin)), 1 * (y + rotateY(shape.e, cos, sin)),

        1 * (x + rotateX(shape.a, cos, sin)), 1 * (y + rotateY(shape.a, cos, sin)))
end

function rotateX(point, cos, sin)
    return point.x * cos - point.y * sin
end

function rotateY(point, cos, sin)
    return point.x * sin + point.y * cos;
end

function UUID()
    local fn = function(x)
        local r = math.random(16) - 1
        r = (x == "x") and (r + 1) or (r % 4) + 9
        return ("0123456789abcdef"):sub(r, r)
    end
    return (("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx"):gsub("[xy]", fn))
end