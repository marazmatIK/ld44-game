Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

LevelManager = Object:extend()

function LevelManager:new(opts)
    self.level = 1
    self.levels = {}
    self.levels[#self.levels + 1] =  {timer = 30, scoresToWin = 60, catsSpawnChance = 0.25}
    self.levels[#self.levels + 1] =  {timer = 25, scoresToWin = 100, catsSpawnChance = 0.25}
    self.levels[#self.levels + 1] =  {timer = 25, scoresToWin = 150, catsSpawnChance = 0.3}
    self.levels[#self.levels + 1] =  {timer = 20, scoresToWin = 200, catsSpawnChance = 0.35 }

    LevelManager.super.new(self, opts)
end

function LevelManager:nextLevel()
    self.level = self.level + 1

    if self.level >= 4 then
        self.level = 1
    end
end

function LevelManager:currentLevel()
    return self.levels[self.level]
end

function LevelManager:currentLevelNumber()
    return self.level
end