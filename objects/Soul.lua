Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

Soul = Collectable:extend()

function Soul:new(opts)
    local props = {x = 200, y = 300, w = TILE_SIZE, h = TILE_SIZE, sprite = love.graphics.newImage("assets/soul_64.png") }
    local opts = opts or {}
    props.x = opts.x or props.x
    props.y = opts.x or props.y

    props.initializingState = opts.initializingState
    props.player = opts.player
    props.muted = opts.muted
    props.static = opts.static
    props.sprite = opts.sprite or props.sprite
    props.type = opts.type or 'Soul'
    props.scoreGainedLabel = 'PLACEHOLDER'


    log.info(opts.initializingState)
    Soul.super.new(self, props)

end

function Soul:getScoreGainedLabel()
    return '+ '..self.player.pointsMultiplier * SCORE_INCREMENT..' \nYEAH! SOMEONE\'S LIFE!'
end

function Soul:playSound()
    SOUND_REGISTRY:playSound('punchSound')
    SOUND_REGISTRY:playSound(self:randomSoundId())
end

function Soul:randomSoundId()
    local rand = love.math.random(1, 3)
    return 'collectedSoul'..tostring(rand)
end