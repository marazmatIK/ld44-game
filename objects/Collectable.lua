Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'
require 'Utils'

    FONT = love.graphics.newFont('assets/fonts/font.ttf', 64)
    SCORE_COLLECTED_FONT = love.graphics.newFont('assets/fonts/font.ttf', 26)

    Collectable = Object:extend()

    function Collectable:new(opts)

        self.type = opts.type
        self.player = opts.player
        self.x = opts.x
        self.y = opts.y
        self.w = opts.w
        self.h = opts.h
        self.sprite = opts.sprite
        self.jumping = false
        self.static = opts.static or false

        self.velocity = {x = COLLECTABLE_VELOCITY, y = COLLECTABLE_VELOCITY }

        self.floorY = self.x
        self.input = {x = 0, y = 0 }
        self.gravity = GRAVITY

        self.color = {r = 0.9, g = 0.01, b = 0.05 }

        self.rect = HC.rectangle(self.x, self.y, self.w, self.h)
        self.rect.target = self

        self.debugMode = opts.debugMode or false

        self.exists = true

        log.info(opts.initializingState)
        self.initializingState = true
        if opts.initializingState then
            self.initializingState = opts.initializingState
        end


        self.canBeDeleted = false
        self.muted = opts.muted or false

        self.timer = Timer()

        self.scoreGainedTimer = Timer()
        self.scoreGainedTextCoordX = self.x
        self.scoreGainedTextCoordY = self.y
        self.scoreGainedTextDead = false
        self.scoreGainedLabel = opts.scoreGainedLabel or '+ '..SCORE_INCREMENTd

        self:changeDirection()
        self.timer:every(3, function()
            self:changeDirection()
        end)

        self.id = UUID()

        Collectable.super.new(self, opts)
    end

    function Collectable:update(dt)
        if self.exists == false then
            if self.scoreGainedTextDead == false then
                self.scoreGainedTimer:update(dt)
            end
            return
        end

        if self.static == false then
            self:updateMovement(dt)

            self.timer:update(dt)

            self:moveFromOtherBorderIfNeeded()
        end
    end

    function Collectable:updateMovement(dt)
        local p = {x = self.x, y = self.y }

        local pos = {x = p.x + self.input.x * self.velocity.x * dt, y = p.y + self.input.y * self.velocity.y * dt }

        self.x = pos.x
        self.y = pos.y

        self:move(self.x, self.y)
    end

    function Collectable:moveFromOtherBorderIfNeeded()
        if self.x >= SCREEN_WIDTH then
            self.x = 0
        end

        if self.x <= - self.w then
            self.x = SCREEN_WIDTH
        end

        if self.y <= -self.h then
            self.y = SCREEN_HEIGHT
        elseif self.y >= SCREEN_HEIGHT then
            self.y = 0
        end
    end

    function Collectable:move(x, y)
        self.rect:moveTo(self.x + self.w / 2, self.y + self.h / 2)
    end

    function Collectable:draw()
        love.graphics.setColor(255 / 255, 255 / 255, 255 / 255, 255)

        if self.exists == false then
            if self.scoreGainedTextDead == false then
                self:drawText(self:getScoreGainedLabel(), self.scoreGainedTextCoordX, self.scoreGainedTextCoordY)
            end
            return
        end
        love.graphics.draw(
            self.sprite,
            math.floor(self.x),
            math.floor(self.y),
            0,
            1,
            1,
            0,
            0
        )

        if self.debugMode then
            -- draw bounding box
            local x1,y1, x2,y2 = self.rect:bbox()
            love.graphics.rectangle('line', x1,y1, x2-x1,y2-y1)
        end

    end

    function Collectable:onCollected()
        if self.initializingState == true then
            log.info('initializing state ? true')
            return
        end
        self.exists = false
        HC.remove(self.rect)

        self.scoreGainedTextCoordX = self.x
        self.scoreGainedTextCoordY = self.y

        if self.muted == false then
            self:playSound()
        end

        self.scoreGainedTimer:tween(1.5, self, {scoreGainedTextCoordY = self.scoreGainedTextCoordY - 100}, 'out-linear', function()
            self.scoreGainedTextDead = true
            self.canBeDeleted = true
        end)
    end

    function Collectable:changeDirection()
        if self.exists then

            self.input.x = love.math.random(-1, 1)
            self.input.y = love.math.random(-1, 1)
        end
    end

    function Collectable:onMove(action_id)
        if action_id =="left" then
            self.input.x = -1
        elseif action_id == "right" then
            self.input.x = 1
        end

        if action_id == "up" then
            self.input.y = 1
        elseif action_id == "down" then
            self.input.y = -1
        end
    end

    function Collectable:setFontColor()
        love.graphics.setColor(25 / 255, 230 / 255, 5 / 255, 255)
    end

    function Collectable:drawText(text, x, y)
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setFont(SCORE_COLLECTED_FONT)
        self:setFontColor()
        love.graphics.print(text, x, y )
        love.graphics.setColor(r, g, b, a)
    end

    function Collectable:keypressed( key, isrepeat )
        if key == '0' then
            self.debugMode = not self.debugMode
        end
    end

    function Collectable:playSound()
        -- noop
    end

function Collectable:getScoreGainedLabel()
    return 'N/A'
end