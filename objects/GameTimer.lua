Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

GAME_TIMER_FONT = love.graphics.newFont('assets/fonts/font.ttf', 26)

GameTimer = Object:extend()

function GameTimer:new(opts)
    self.x = opts.x
    self.y = opts.y
    self.w = opts.w
    self.fullW = opts.w
    self.h = opts.h
    self.decrementSize = 20
    self.pauzed = true
    self.duration = 3

    self.timer = Timer()

    GameTimer.super.new(self, opts)
end

function GameTimer:reStart(duration, onExpired)
    if self.timerHandle then
        Timer.cancel(self.timerHandle)
    end
    self.timer = Timer()
    self.duration = duration
    self.pauzed = false
    self.timerHandle = self.timer:after(duration, function()
        if onExpired then
            onExpired()
        end
    end)
end

function GameTimer:update(dt)
    if self.pauzed == true then
        return
    end
    local decrementSize = self.fullW * dt / self.duration
    self.timer:update(dt)
    self.w = self.w - decrementSize
    if self.w <= 0 then
        self.w = 0
    end
end

function GameTimer:draw()

    local color = self:resolveColor(true)
    love.graphics.setColor(color.r, color.g, color.b, 255)

    love.graphics.rectangle("fill", self.x, self.y, self.fullW, self.h )

    local color = self:resolveColor(false)
    love.graphics.setColor(color.r, color.g, color.b, 255)

    love.graphics.rectangle("fill", self.x, self.y, self.w, self.h )

    love.graphics.setColor(1, 1, 1, 255)
    self:drawText('Time:', self.x - 70, self.y - 5)

end

function GameTimer:resolveColor(background)
    if self.w / self.fullW >= 0.6 then
        if background == true then
            return {r = 47 / 255, g = 79 / 255, b = 79 / 255 }
        else
            return {r = 144 / 255, g = 238 / 255, b = 144 / 255 }
        end
    end

    if self.w / self.fullW >= 0.25 then
        if background == true then
            return {r = 128 / 255, g = 128 / 255, b = 0 / 255 }
        else
            return {r = 255 / 255, g = 215 / 255, b = 0 / 255 }
        end
    end

    if background == true then
        return {r = 128 / 255, g = 0 / 255, b = 0 / 255 }
    else
        return {r = 255 / 255, g = 69 / 255, b = 0 / 255 }
    end
end

function GameTimer:onCollected()

end


function GameTimer:drawText(text, x, y)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setFont(GAME_TIMER_FONT)
    love.graphics.setColor(255 / 255, 155 / 255, 0, 255)
    love.graphics.print(text, x, y )
    love.graphics.setColor(r, g, b, a)
end