Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

PointsBooster = Collectable:extend()

function PointsBooster:new(opts)
    local props = {x = 200, y = 300, w = TILE_SIZE, h = TILE_SIZE, sprite = love.graphics.newImage("assets/points_boost_64.png") }
    local opts = opts or {}
    props.x = opts.x or props.x
    props.y = opts.x or props.y

    props.initializingState = opts.initializingState
    props.player = opts.player
    props.muted = opts.muted
    props.static = opts.static
    props.sprite = opts.sprite or props.sprite
    props.type = opts.type or 'PointsBooster'
    props.scoreGainedLabel = opts.scoreGainedLabel or 'x2 POINTS BOOST!!\nREAP YEAH!'


    log.info(opts.initializingState)
    PointsBooster.super.new(self, props)

end

function PointsBooster:getScoreGainedLabel()
    return 'x2 TO POINTS!!\nREAP YEAH!'
end

function PointsBooster:playSound()
    SOUND_REGISTRY:playSound(self:randomSoundId())
end

function PointsBooster:randomSoundId()
    return 'pointsBoost'..tostring(1)
end