require "Dependencies"

Object = require 'lib/classic/classic'
require 'objects/Player'
require 'objects/HudIndicator'
require 'objects/Collectable'
require 'objects/Soul'
require 'objects/Kitty'
require 'objects/PointsBooster'
require 'objects/SoulEmitter'

require 'objects/GameTimer'


require 'Constants'
require 'Dependencies'


FONT = love.graphics.newFont('assets/fonts/font.ttf', 64)
FONT_HUD = love.graphics.newFont('assets/fonts/font.ttf', 24)
FONT_SMALL = love.graphics.newFont('assets/fonts/font.ttf', 24)

Game = Object:extend()

function Game:new(opts)

    self.cam = Camera(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
    self.gameOver = false
    self.hud = opts.hud
    self.player = Player({x = SCREEN_WIDTH / 2, y = SCREEN_HEIGHT / 2, w = TILE_SIZE,
        h = TILE_SIZE,
        sprite = love.graphics.newImage("assets/player_64.png"),
        cam = self.cam, scoresToWin = opts.scoresToWin or 600})

    self.SOULS_EMITTER = SoulEmitter({player = self.player})

    Game.super.new(self, opts)

end

function Game:startGame()
    self.SOULS_EMITTER:addSoul()
    self.SOULS_EMITTER:addKitty()
    self.SOULS_EMITTER:addSoul()
    self.SOULS_EMITTER:addKitty()

    self.timer = Timer()

    self.player.points = 0

    self.gameTimer = GameTimer({x = 80, y = SCREEN_HEIGHT - 40, w = SCREEN_WIDTH - 40 - 80, h = 20})

    self:reStart(30)


    self.active = true

    self:startMusic()
end

function Game:startMusic()
    SOUND_REGISTRY:stopSound('inGameTrack1')
    SOUND_REGISTRY:playSound('inGameTrack1')
end


function Game:clearResources()
    self.SOULS_EMITTER:clear()
end

function Game:reStart(duration)
    self.gameTimer:reStart(duration, function()
        self.gameOver = true
        log.warn('Game over')
    end)
end

function Game:keypressed( key, isrepeat )
    if self.active == false then
        return
    end
    if key == 'h' then
        self.showHelp = not self.showHelp
    end

    if key == "0" then
        self.player:keypressed("0")
        if self.SOULS_EMITTER.souls then
            for _, soul in pairs(self.SOULS_EMITTER.souls) do
                soul:keypressed("0")
            end
        end
    end
end

function Game:keyreleased( key )
    if key == 'a' or key == 'd' or key == 'space' then

    end
end

function Game:update(dt)

    if self.active == false then
        return
    end

    if self.gameTimer then
        self.gameTimer:update(dt)
    end
    if self.timer then
        self.timer:update(dt)
    end
    self:handleKeys()

    self.player:update(dt)
    self.SOULS_EMITTER:updateSouls(dt)

    local collisions = HC.collisions(self.player.weaponCircle)
    for other, separating_vector in pairs(collisions) do
        local x, y = other:center()
        x = x - TILE_SIZE / 2
        y = y - TILE_SIZE / 2

        if self.gameOver == false and other and other.target and other.target.initializingState == false then
            other.target:onCollected()
            self.player:onCollected(other.target)

            self.SOULS_EMITTER:addRandomSoul()
        end
    end
end

function Game:handleKeys()
    if self.active == false then
        return
    end
    if love.keyboard.isDown("a") then
        self.player:onKeyDown("a")
    end
    if love.keyboard.isDown("w") then
        self.player:onKeyDown("w")
    end
    if love.keyboard.isDown("s") then
        self.player:onKeyDown("s")
    end
    if love.keyboard.isDown("d") then
        self.player:onKeyDown("d")
    end
    if love.keyboard.isDown("space") then
        self.player:onKeyDown("space")
    end
    if love.keyboard.isDown("x") then
        self.player:onKeyDown("x")
    end
    if love.keyboard.isDown("y") then
        self.player:onKeyDown("y")
    end
    if love.keyboard.isDown("r") then
        self.player:onKeyDown("r")
    end
end

function Game:draw()

    if self.active == false then
        return
    end

    self.cam:attach()

    for i = #self.SOULS_EMITTER.souls, 1, -1 do
        local soul = self.SOULS_EMITTER.souls[i]
        soul:draw()
    end

    self.player:draw()

    if  self.showHelp == true then
        drawGameSmallText("HOLD [ A ] - counterclock-wise aiming  HOLD [ D ] - clock-wise aiming\nKEY RELEASED LAUNCHES A MISSLE\nPRESS [ F ] TO PAY RESPECT\nPRESS [ H ] TO HIDE/UNHIDE THE GUIDE", 20, love.graphics.getHeight() - 70)
    end
    self.cam:detach()


    self:drawHud(self.player)
    if self.gameTimer then
        self.gameTimer:draw()
    end
    --self:debugPrint())

    if self.gameOver == true then
        if self.player:isWinner() then
            drawGameText('MISSION COMPLETE!\nPRESS [ ENTER ]\nto goto next level\n\nPRESS [ F ]\nto restart level', SCREEN_WIDTH / 5, SCREEN_HEIGHT / 3)
        else
            drawGameText('GAME OVER\nPRESS [ F ]\nto restart level', SCREEN_WIDTH / 5, SCREEN_HEIGHT / 3)
        end
    end

end

function Game:debugPrint()
    if self.active == false then
        return
    end
    drawGameSmallText(string.format("Debug info here"), 20,  70)
end

function Game:drawHud(player)
    if self.hud then
        self.hud:draw(player)
    end
end

function drawGameText(text, x, y)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setFont(FONT)
    love.graphics.setColor(255 / 255, 155 / 255, 0, 255)
    love.graphics.print(text, x, y )
    love.graphics.setColor(r, g, b, a)
end

function drawGameSmallText(text, x, y)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setFont(FONT_SMALL)
    love.graphics.setColor(255 / 255, 155 / 255, 0, 255)
    love.graphics.print(text, x, y )
    love.graphics.setColor(r, g, b, a)
end