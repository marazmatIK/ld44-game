Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

SoulEmitter = Object:extend()

function SoulEmitter:new(opts)
    HC.resetHash()
    local opts = opts or {}
    self.player = opts.player
    self.souls = {}

    SoulEmitter.super.new(self, opts)
end

function SoulEmitter:clear()
    for i = #self.souls, 1, -1 do
        table.remove(self.souls, i)
    end
    HC.resetHash()

end

function SoulEmitter:addSoul()
    return self:registerSoul('Soul')
end

function SoulEmitter:addKitty()

    return self:registerSoul('Kitty')
end

function SoulEmitter:addPointsBooster()

    return self:registerSoul('PointsBooster')
end

function SoulEmitter:addRandomSoul()
    if  #self.souls > MAX_SOULS_COUNT then
        return
    end
    local rand = love.math.random()
    -- TODO: need to use LevelManager data here!!!!!
    if rand < POINTS_BOOST_CHANCE then
        return self:addPointsBooster()
    elseif rand < CATS_SPAWN_CHANCE then
        return self:addKitty()
    else
        return self:addSoul()
    end

end

function SoulEmitter:updateSouls(dt)
    for i = #self.souls, 1, -1 do
        local soul = self.souls[i]
        soul:update(dt)
        if soul.canBeDeleted == true then
            table.remove(self.souls, i)
        end
    end
end

function SoulEmitter:registerSoul(soulClass)
    local soul = {}
    local collisions = {}
    local randomX = self:randomX()
    local randomY = self:randomY()

    local opts = {}
    opts.x = randomX
    opts.y = randomY
    opts.player = self.player

    soul = _G[soulClass](opts)

    collisions = HC.collisions(soul.rect)

    for i = 1, 20 do

        if tablelength(collisions) > 0 or self:closeToPlayer(soul) == true then

             randomX = self:randomX()
             randomY = self:randomY()

            local opts = {}
            opts.x = randomX
            opts.y = randomY
            opts.player = self.player
            soul = _G[soulClass](opts)

            collisions = HC.collisions(soul.rect)
        else
            break
        end

    end

    self.souls[#self.souls + 1] = soul

    soul.initializingState = false

    return soul
end

function SoulEmitter:randomX()
    local randomX1 = love.math.random(10, (SCREEN_WIDTH / 2 - 3 * TILE_SIZE))
    local randomX2 = love.math.random((SCREEN_WIDTH / 2 + 3 * TILE_SIZE), (SCREEN_WIDTH - TILE_SIZE))
    if love.math.random() < 0.5 then
        return randomX1
    else
        return randomX2
    end
end

function SoulEmitter:randomY()
    local randomY1 = love.math.random(10, (SCREEN_HEIGHT / 2 - 3 * TILE_SIZE))
    local randomY2 = love.math.random((SCREEN_HEIGHT / 2 + 3 * TILE_SIZE), (SCREEN_HEIGHT - TILE_SIZE))
    if love.math.random() < 0.5 then
        return randomY1
    else
        return randomY2
    end
end

function SoulEmitter:closeToPlayer(soul)
    if (soul.x - self.player.x > TILE_SIZE * 2) or (soul.x - self.player.x <= -TILE_SIZE * 2) then
        return true
    end
    if (soul.y - self.player.y > TILE_SIZE * 2) or (soul.y - self.player.y <= -TILE_SIZE * 2) then
        return true
    end

    return false
end

function tablelength(T)
    local count = 0
    for _ in pairs(T) do count = count + 1 end
    return count
end