Object = require 'lib/classic/classic'
require 'Constants'

FONT = love.graphics.newFont('assets/fonts/font.ttf', 64)
FONT_INTRO = love.graphics.newFont('assets/fonts/font.ttf', 30)

Introducer = Object:extend()

function Introducer:new(opts)
    self.x = 10
    self.y = 10
    if opts then
        self.x = opts.x or 10
        self.y = opts.y or 10

        self.color = {r = 1, g = 1, b = 1}
    end
    self.step = 0
    self.active = true
    self.onIntroEnd = opts.onIntroEnd

    self.playerX = self.x + 20
    self.playerXOrigin = self.playerX
    self.playerY = self.y + 240
    self.playerEndX = self.x + 220
    self.playerPathLength = self.playerEndX - self.playerXOrigin
    self.playerTimer = Timer()
    self.playerDirection = 1

    self.resurrectTimer = Timer()

    self.player = Player({x = self.playerX, y = self.playerY, w = TILE_SIZE, h = TILE_SIZE, sprite = love.graphics.newImage("assets/player_64.png"),
        scoresToWin = opts.scoresToWin or 600})

    self.soul = Soul({x = self.playerEndX, y = self.playerY, w = TILE_SIZE, h = TILE_SIZE, static = true,
        muted = true, initializingState = false})
    self.soul.x = self.soul.x + 40

    self.kitty = Kitty({x = self.playerEndX, y = self.playerY, w = TILE_SIZE, h = TILE_SIZE, static = true,
        muted = true, initializingState = false})
    self.kitty.x = self.soul.x + 40
    self.kitty.y = self.kitty.y + TILE_SIZE

    self.soulStatic = Soul({x = self.playerEndX, y = self.playerY, w = TILE_SIZE, h = TILE_SIZE, static = true,
        muted = true, initializingState = false})
    self.soulStatic.x = self.kitty.x - TILE_SIZE * 2
    self.soulStatic.y = self.soulStatic.y + TILE_SIZE


    self.noSprite = love.graphics.newImage("assets/introducer/icon_no_32.png")
    self.yesSprite = love.graphics.newImage("assets/introducer/icon_yes_32.png")

    self.aButton = love.graphics.newImage("assets/introducer/button_a_32.png")
    self.wButton = love.graphics.newImage("assets/introducer/button_w_32.png")
    self.dButton = love.graphics.newImage("assets/introducer/button_d_32.png")
    self.sButton = love.graphics.newImage("assets/introducer/button_s_32.png")
    self.fButton = love.graphics.newImage("assets/introducer/button_f_32.png")

    self.buttonYDelta = 0
    self.buttonYDeltaEnd = 20
    self.buttonYDeltaDirection = 1

    self.textYDelta = 0
    self.textYDeltaEnd = 40
    self.textYDeltaDirection = -1

    Introducer.super.new(self, opts)
end

function Introducer:startMusic()
    SOUND_REGISTRY:stopSound('inPauzeTrack1')
    SOUND_REGISTRY:playSound('inPauzeTrack1')
end

function Introducer:stopMusic()
    SOUND_REGISTRY:stopSound('inPauzeTrack1')
end


function Introducer:update(dt)
    if self.active == false then
        return
    end
    if self.step >= 5 then
        self.active = false
        self.onIntroEnd()
        return
    end
    self.soul:update(dt)
    self.kitty:update(dt)

    self.player:update(dt)
    self.playerTimer:update(dt)
    self.resurrectTimer:update(dt)

    if self.step < 4 then
        self:movePlayerBackAndForth(dt)
    elseif self.step == 4 then
        self:movePlayerHorizontalyInfinitely(dt)
    end

    self:updateButonYDelta(dt)
    self:updateTextYDelta(dt)
end

function Introducer:updateButonYDelta(dt)
    self.buttonYDelta = self.buttonYDelta + dt * 40 * self.buttonYDeltaDirection
    if self.buttonYDelta >= self.buttonYDeltaEnd then
        self.buttonYDeltaDirection = -1
    elseif self.buttonYDelta <= 0 then
        self.buttonYDeltaDirection = 1
    end
end

function Introducer:updateTextYDelta(dt)
    self.textYDelta = self.textYDelta + dt * 40 * self.textYDeltaDirection
    if self.textYDelta >= self.textYDeltaEnd then
        self.textYDeltaDirection = -1
    elseif self.textYDelta <= 0 then
        self.textYDeltaDirection = 1
    end
end

function Introducer:movePlayerHorizontalyInfinitely(dt)
    self.player.x = self.player.x + PLAYER_MAX_VELOCITY * dt
end

function Introducer:movePlayerBackAndForth(dt)
    self.player.x = self.player.x + self.playerPathLength * dt * self.playerDirection
    if self.player.x >= self.playerEndX then
        self.playerDirection = -1
        self.soul:onCollected()

        self.resurrectTimer:after(1.2, function()
            self.soul = Soul({x = self.playerEndX, y = self.playerY, w = TILE_SIZE, h = TILE_SIZE, static = true, muted = true,
                initializingState = false})
            self.soul.x = self.soul.x + 40
        end)
    end
    if self.player.x <= self.playerXOrigin then
        self.playerDirection = 1
    end
end

function Introducer:draw()
    if self.active == false then
        return
    end
    if self.step == 0 then
        local text = 'Hello, pal ! \nIn this game\nyou nead to reap living souls\nBecause life is currency!\n\nPress [ ENTER ] for next step'
        self:drawText(text, self.x, self.y)
    elseif self.step == 1 then


        local text = 'You control The Reaper\nwho doomed to collect\nliving souls..FOREVER\n\nPress [ ENTER ] for next step'
        self:drawText(text, self.x, self.y)

        self.soul:draw()
        self.player:draw()

    elseif self.step == 2 then


        local text = 'Collect human souls\nTry not to touch\nKitten Souls!!!!!\nOtherwise You will get\nScores Penalty\n\nPress [ ENTER ] for next step'
        self:drawText(text, self.x, self.y)

        self.kitty:draw()
        love.graphics.draw(
            self.noSprite,
            math.floor(self.kitty.x - TILE_SIZE / 2),
            math.floor(self.kitty.y),
            0,
            1,
            1,
            0,
            0
        )

        self.soulStatic:draw()
        love.graphics.draw(
            self.yesSprite,
            math.floor(self.soulStatic.x - TILE_SIZE / 2),
            math.floor(self.soulStatic.y),
            0,
            1,
            1,
            0,
            0
        )

    elseif self.step == 3 then
        local text = 'Use [ W-A-S-D ] buttons\nTo move the reaper\nUse [ F ] to restart the Game\n\nPress [ ENTER ] for next step'
        self:drawText(text, self.x, self.y)

        love.graphics.draw(
            self.aButton,
            math.floor(self.x),
            math.floor(self.buttonYDelta + self.y + 240),
            0,
            1,
            1,
            0,
            0
        )
        love.graphics.draw(
            self.wButton,
            math.floor(self.x + TILE_SIZE),
            math.floor(self.buttonYDelta + self.y + 240 - TILE_SIZE),
            0,
            1,
            1,
            0,
            0
        )
        love.graphics.draw(
            self.dButton,
            math.floor(self.x + TILE_SIZE * 2),
            math.floor(self.buttonYDelta + self.y + 240),
            0,
            1,
            1,
            0,
            0
        )
        love.graphics.draw(
            self.sButton,
            math.floor(self.x + TILE_SIZE),
            math.floor(self.buttonYDelta + self.y + 240),
            0,
            1,
            1,
            0,
            0
        )

        love.graphics.draw(
            self.fButton,
            math.floor(self.x + 6 * TILE_SIZE),
            math.floor(self.buttonYDelta + self.y + 240),
            0,
            1,
            1,
            0,
            0
        )

        self.player:draw()

    elseif self.step == 4 then
        local text = 'If you reach screen border\nthat\'s fine!\nYou will just float out\nfrom the opposite one\n\nPress [ ENTER ] to Start The Game!'
        self:drawText(text, self.x, self.y)

        self.player:draw()
    end
end

function Introducer:keypressed( key, isrepeat )
    if self.active == false then
        return
    end
    if key == 'return' then
        self.step = self.step + 1
    end
    if key == 'space' then
        self.step = self.step + 5
    end
end

function Introducer:drawText(text, x, y)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setFont(FONT_INTRO)
    --love.graphics.setColor(255 / 255, 155 / 255, 0, 255)
    love.graphics.print(text, x, y + self.textYDelta )
    love.graphics.setColor(r, g, b, a)
end