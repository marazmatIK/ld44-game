Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

Kitty = Soul:extend()

function Kitty:new(opts)
    local props = {sprite = love.graphics.newImage("assets/kitty_64.png") }
    local opts = opts or props
    opts.sprite = opts.sprite or props.sprite
    opts.type = 'Kitty'
    opts.player = opts.player or props.player

    opts.scoreGainedLabel = '- '..SCORE_INCREMENT..' \nNO! DON\'T KILL\nKITTIES!' or opts.scoreGainedLabel
    Kitty.super.new(self, opts)
end

function Kitty:calculateAngleBetween(objA, objB)
    local angle = math.atan2(objB.y - objA.y, objB.x - objA.x)
    return angle
end

function Kitty:getScoreGainedLabel()
    return '- '..SCORE_INCREMENT..' \nNO! DON\'T KILL\nKITTIES!'
end

function Kitty:calculateAngle()
    local randomAngleInRadians = love.math.random(-math.pi / 12, math.pi / 12)

    local angleBetweenPlayerAndSelf = self:calculateAngleBetween(self, self.player)

    angleBetweenPlayerAndSelf = angleBetweenPlayerAndSelf + randomAngleInRadians

    return angleBetweenPlayerAndSelf
end

function Kitty:updateMovement(dt)

    local angle = self:calculateAngle()

    local p = {x = self.x, y = self.y }

    local pos = {x = p.x + math.cos(angle) * self.velocity.x * dt, y = p.y + math.sin(angle) * self.velocity.y * dt }

    self.x = pos.x
    self.y = pos.y

    self:move(self.x, self.y)
end

function Kitty:setFontColor()
    love.graphics.setColor(255 / 255, 25 / 255, 0, 255)
end

function Kitty:playSound()
    SOUND_REGISTRY:playSound('punchSound')
    SOUND_REGISTRY:playSound(self:randomSoundId())
end

function Kitty:randomSoundId()
    local rand = love.math.random(1, 5)
    return 'collectedKitty'..tostring(rand)
end