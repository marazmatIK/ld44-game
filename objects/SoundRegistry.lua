require "Dependencies"

SoundRegistry = Object:extend()

function SoundRegistry:new(opts)
    self.sounds = {collectedSoul1 = love.audio.newSource("assets/sounds/game/soul-collected1.ogg", "static"),
        collectedSoul2 = love.audio.newSource("assets/sounds/game/soul-collected2.wav", "static"),
        collectedSoul3 = love.audio.newSource("assets/sounds/game/soul-collected3.wav", "static"),

        collectedKitty1 = love.audio.newSource("assets/sounds/game/kitty-collected1.wav", "static"),
        collectedKitty2 = love.audio.newSource("assets/sounds/game/kitty-collected2.wav", "static"),
        collectedKitty3 = love.audio.newSource("assets/sounds/game/kitty-collected3.wav", "static"),
        collectedKitty4 = love.audio.newSource("assets/sounds/game/kitty-collected4.wav", "static"),
        collectedKitty5 = love.audio.newSource("assets/sounds/game/kitty-collected5.wav", "static"),

        inGameTrack1 = love.audio.newSource("assets/sounds/game/in-game-track1.ogg", "static"),
        inPauzeTrack1 = love.audio.newSource("assets/sounds/game/in-pauze-track.ogg", "static"),

        pointsBoost1 = love.audio.newSource("assets/sounds/game/points_boost1.wav", "static"),

        punchSound = love.audio.newSource("assets/sounds/game/punch_sound.ogg", "static")
    }

    SoundRegistry.super.new(self, opts)
end

function SoundRegistry:stopSound(id)
    local sound = self.sounds[id]
    if sound then
        love.audio.stop( sound )
    end
end

function SoundRegistry:sound(id)
    return self.sounds[id]
end

function SoundRegistry:playSound(id)
    local sound = self.sounds[id]
    if sound then
        sound:play()
    end
end

SOUND_REGISTRY = SoundRegistry()