Object = require 'lib/classic/classic'
require 'Constants'

FONT = love.graphics.newFont('assets/fonts/font.ttf', 64)
FONT_HUD = love.graphics.newFont('assets/fonts/font.ttf', 30)

HudIndicator = Object:extend()

function HudIndicator:new(opts)
    self.x = 10
    self.y = 10
    self.levelManager = opts.levelManager
    if opts then
        self.x = opts.x or 10
        self.y = opts.y or 10

        self.color = {r = 1, g = 1, b = 1}
    end

    HudIndicator.super.new(self, opts)
end

function HudIndicator:draw(player)
    local id = ''
    local text = 'Money earned: '..player.points
    drawHud(text, self.x, self.y)

    drawHud('Scores to win: '..player.scoresToWin, self.x, self.y + 40)
    drawHud('LEVEL: '..tostring(self.levelManager:currentLevelNumber()), SCREEN_WIDTH - 120, self.y)

end

function drawHud(text, x, y)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setFont(FONT_HUD)
    love.graphics.setColor(255 / 255, 155 / 255, 0, 255)
    love.graphics.print(text, x, y )
    love.graphics.setColor(r, g, b, a)
end