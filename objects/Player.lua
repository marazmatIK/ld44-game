Object = require 'lib/classic/classic'
require 'Constants'
require 'Dependencies'

Player = Object:extend()

function Player:new(opts)

    self.type = opts.type or 'Player'
    self.x = opts.x
    self.y = opts.y
    self.w = opts.w
    self.h = opts.h
    self.sprite = opts.sprite
    self.cam = opts.cam
    self.scoresToWin = opts.scoresToWin

    self.velocity = {x = 0, y = 0}
    self.floorY = self.x
    self.input = {x = 0, y = 0 }

    self.color = {r = 0.9, g = 0.01, b = 0.05 }

    self.rect = HC.rectangle(self.x + 4, self.y, self.w - 4, self.h)
    self.rect.target = self

    self.cameraX = SCREEN_WIDTH / 2
    self.cameraY = SCREEN_HEIGHT / 2
    self.gameOver = false
    self.debugMode = opts.debugMode or false

    self.weaponHeight = 96
    self.weaponWidth = 32
    self.weaponX = self.x + self.w / 2 - self.weaponWidth / 4
    self.weaponY = self.y + self.h / 2 - self.weaponHeight

    self.weaponAngle = -math.pi

    self.weaponRadius = self.weaponHeight / 3
    self.weaponOuterRadius = self.weaponHeight * 0.9

    local wRect = self:adjustWeaponRect()
    self.weaponCircle = HC.circle(wRect.x, wRect.y, self.weaponRadius)

    self.weaponSprite = love.graphics.newImage("assets/spit.png")


    self.pointsMultiplier = 1


    self.points = 0
    self.collected = {}
    Player.super.new(self, opts)
end

function Player:isWinner()
    return self.points >= self.scoresToWin
end

function Player:update(dt)
    local acceleration = {x = self.input.x * 400, y = self.input.y * 400}
    local p = {x = self.x, y = self.y }

    self.velocity.y = self.velocity.y + acceleration.y * dt
    self.velocity.x = self.velocity.x + acceleration.x * dt

    if self.velocity.y > PLAYER_MAX_VELOCITY then
        self.velocity.y = PLAYER_MAX_VELOCITY
    end
    if self.velocity.y < -PLAYER_MAX_VELOCITY then
        self.velocity.y = -PLAYER_MAX_VELOCITY
    end

    if self.velocity.x > PLAYER_MAX_VELOCITY then
        self.velocity.x = PLAYER_MAX_VELOCITY
    end
    if self.velocity.x < -PLAYER_MAX_VELOCITY then
        self.velocity.x = -PLAYER_MAX_VELOCITY
    end

    local pos = {x = p.x + self.velocity.x * dt, y = p.y - self.velocity.y * dt }

    self.x = pos.x
    self.y = pos.y

    self.weaponX = self.x + self.w / 2 - self.weaponWidth / 4
    self.weaponY = self.y + self.h / 2

    -- move player collider object to new position
    self.rect:moveTo(self.x + self.w / 2, self.y + self.h / 2)

    self.input = {x = 0, y = 0 }

    self.weaponAngle = self.weaponAngle - ANGLE_VELOCITY * dt


    local wRect = self:adjustWeaponRect()

    self.weaponCircleX = wRect.x
    self.weaponCircleY = wRect.y

    self.weaponCircle:moveTo(self.weaponCircleX, self.weaponCircleY)

    if self.x >= SCREEN_WIDTH then
        self.x = 0
    end

    if self.x <= - self.w then
        self.x = SCREEN_WIDTH
    end

    if self.y <= -self.h then
        self.y = SCREEN_HEIGHT
    elseif self.y >= SCREEN_HEIGHT then
        self.y = 0
    end

end

function Player:onCollected(soul)
    if not soul then
        return
    end

    if soul and soul.type == 'Soul' then
        self.points = self.points + self.pointsMultiplier * SCORE_INCREMENT
    elseif soul and soul.type == 'Kitty' then
        self.points = self.points - SCORE_INCREMENT
    elseif soul and soul.type == 'PointsBooster' then
        self.pointsMultiplier = 2
        self:resetPointsMultiplierAfter(7)
    end
end

function Player:resetPointsMultiplierAfter(resetAfterSeconds)
    local resetTimer = Timer()
    log.info(self.pointsMultiplier)
    resetTimer:after(resetAfterSeconds, function()
        self.pointsMultiplier = 1
        log.info(self.pointsMultiplier)
    end)
end

function Player:adjustWeaponRect()
    local x = self.x + (self.w / 2 ) + self.weaponOuterRadius * math.cos(self.weaponAngle + math.pi / 2 - math.pi / 12 )
    local y = self.y + (self.h / 2) + self.weaponOuterRadius * math.sin(self.weaponAngle + math.pi / 2 - math.pi / 12)
    return {x = x, y = y, w = self.weaponHeight / 3, h = self.weaponHeight / 3 }
end

function Player:draw()
    love.graphics.setColor(255 / 255, 255 / 255, 255 / 255, 255)

    love.graphics.draw(
        self.weaponSprite,
        math.floor(self.weaponX),
        math.floor(self.weaponY),
        self.weaponAngle,
        1,
        1,
        0,
        0
    )

    love.graphics.draw(
        self.sprite,
        math.floor(self.x),
        math.floor(self.y),
        0,
        1,
        1,
        0,
        0
    )

    self:drawDebugBoxes()

end

function Player:drawDebugBoxes()
    if self.debugMode then
        -- draw bounding box
        local x1,y1, x2,y2 = self.rect:bbox()
        love.graphics.rectangle('line', x1,y1, x2-x1,y2-y1)

        x1,y1, x2,y2 = self.weaponCircle:bbox()
        love.graphics.circle('line', self.weaponCircleX, self.weaponCircleY, self.weaponRadius)

        love.graphics.circle('fill', self.weaponX, self.weaponY, 8)

        love.graphics.circle('fill', self.x, self.y, 8)
    end
end

function Player:onMove(action_id)
    if action_id =="left" then
        self.input.x = -1
    elseif action_id == "right" then
        self.input.x = 1
    end

    if action_id == "up" then
        self.input.y = 1
    elseif action_id == "down" then
        self.input.y = -1
    end
end

function Player:keypressed( key, isrepeat )
    if self.gameOver == true then
        return
    end
    if key == 'a' then
        self:onMove('left')
    end
    if key == 'd' then
        self:onMove('right')
    end
    if key == 'w' then
        self:onMove('up')
    end
    if key == 's' then
        self:onMove('down')
    end
    if key == '0' then
        self.debugMode = not self.debugMode
    end
end

function Player:onKeyDown(key)
    if self.gameOver == true then
        return
    end
    if key == 'a' then
        self:onMove('left')
    end
    if key == 'd' then
        self:onMove('right')
    end
    if key == 'w' then
        self:onMove('up')
    end
    if key == 's' then
        self:onMove('down')
    end
end